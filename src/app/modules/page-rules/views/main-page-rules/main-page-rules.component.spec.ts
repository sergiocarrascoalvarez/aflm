import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageRulesComponent } from './main-page-rules.component';
import { SharedModule } from '../../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';

describe('MainPageRulesComponent', () => {
  let component: MainPageRulesComponent;
  let fixture: ComponentFixture<MainPageRulesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule
      ],
      declarations: [ MainPageRulesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageRulesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
