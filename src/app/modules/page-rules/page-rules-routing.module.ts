import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { MainPageRulesComponent } from './views/main-page-rules/main-page-rules.component';

const routes: Routes = [
  {
    path: '',
    component: MainPageRulesComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageRulesRoutingModule { }
