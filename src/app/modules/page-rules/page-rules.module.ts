import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { PageRulesRoutingModule } from './page-rules-routing.module';

// Shared
import { SharedModule } from '../shared/shared.module';

// Components
import { MainPageRulesComponent } from './views/main-page-rules/main-page-rules.component';

@NgModule({
  declarations: [MainPageRulesComponent],
  imports: [
    CommonModule,
    PageRulesRoutingModule,
    SharedModule
  ]
})
export class PageRulesModule { }
