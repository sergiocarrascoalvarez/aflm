import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Componets
import { MainDetailComponent } from './views/main-detail/main-detail.component';
import { RulesGuard } from 'src/app/guards/rules.guard';

const routes: Routes = [
  {
    path: 'animals',
    component: MainDetailComponent,
    data: { category: 'animals'},
    canActivate: [RulesGuard]
  },
  {
    path: 'books',
    component: MainDetailComponent,
    data: { category: 'books'},
    canActivate: [RulesGuard]
  },
  {
    path: 'music',
    component: MainDetailComponent,
    data: { category: 'music'},
    canActivate: [RulesGuard]
  },
  {
    path: 'healt',
    component: MainDetailComponent,
    data: { category: 'healt'},
    canActivate: [RulesGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailRoutingModule { }
