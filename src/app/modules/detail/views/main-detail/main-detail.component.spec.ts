import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainDetailComponent } from './main-detail.component';
import { InfoDetailComponent } from '../../components/info-detail/info-detail.component';
import { TableDetailComponent } from '../../components/table-detail/table-detail.component';
import { SharedModule } from '../../../shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientModule } from '@angular/common/http';
import { Link } from 'src/app/entities/link';
import { LinksService } from '../../../../providers/links/links.service';
import { of } from 'rxjs';

describe('MainDetailComponent', () => {
  let component: MainDetailComponent;
  let fixture: ComponentFixture<MainDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule,
        RouterTestingModule,
        HttpClientModule
      ],
      declarations: [
        MainDetailComponent,
        InfoDetailComponent,
        TableDetailComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should updateActiveLink', () => {
    const link: Link = {
      name: 'name',
      description: 'description',
      url: 'www.google.es',
      author: 'author'
    };
    component.updateActiveLink(link);
  });

  it('should getTitle animals', () => {
    component.getTitle('animals');
  });

  it('should getTitle music', () => {
    component.getTitle('music');
  });

  it('should getTitle books', () => {
    component.getTitle('books');
  });

  it('should getTitle healt', () => {
    component.getTitle('healt');
  });

  it('should getData', () => {
    const links: Link[] = [
      {
        name: 'name',
        description: 'description',
        url: 'www.google.es',
        author: 'author'
      }
    ];
    const de = fixture.debugElement;
    const service = de.injector.get(LinksService);
    spyOn(service, 'getLinks').and.returnValue(of(links));
    component.getData('animals');
  });
});
