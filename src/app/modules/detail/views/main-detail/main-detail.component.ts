import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LinksService } from '../../../../providers/links/links.service';
import { Subscription } from 'rxjs';
import { Link } from 'src/app/entities/link';
import { RulesService } from '../../../../providers/rules/rules.service';

@Component({
  selector: 'app-main-detail',
  templateUrl: './main-detail.component.html',
  styleUrls: ['./main-detail.component.css']
})
export class MainDetailComponent implements OnInit, OnDestroy {

  links: Link[] = [];
  activeLink: Link = {
    name: '',
    description: '',
    author: '',
    url: ''
  };
  title: string;

  private category: string;

  private subscription: Subscription;

  constructor(
    private router: Router,
    private linksService: LinksService,
    private rulesService: RulesService
  ) { }

  ngOnInit() {
    this.getPath();
    this.getData(this.category);
    this.getTitle(this.category);
    this.rulesService.newVisitedPage(this.category);
  }

  private getPath(): void {
    const path = this.router.url.split('/');
    const category = path[path.length - 1];
    this.category = category;
  }

  getData(category: string): void {
    this.subscription = this.linksService.getLinks(category).subscribe(links => {
      this.links = links;
    });
  }

  getTitle(category: string): void {
    switch (category) {
      case 'animals':
        this.title = 'animales';
        break;
      case 'books':
        this.title = 'libros';
        break;
      case 'music':
        this.title = 'música';
        break;
      case 'healt':
        this.title = 'salud';
        break;
      default:
        break;
    }
  }

  updateActiveLink(link: Link): void {
    this.activeLink = link;
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
