import { Component, OnInit, Input } from '@angular/core';
import { Link } from 'src/app/entities/link';

@Component({
  selector: 'app-info-detail',
  templateUrl: './info-detail.component.html',
  styleUrls: ['./info-detail.component.css']
})
export class InfoDetailComponent implements OnInit {

  @Input() activeLink: Link;

  constructor() { }

  ngOnInit() {
  }

  goToLink(url: string) {
    window.open(url, '_blank');
  }

}
