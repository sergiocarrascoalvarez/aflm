import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Link } from 'src/app/entities/link';

@Component({
  selector: 'app-table-detail',
  templateUrl: './table-detail.component.html',
  styleUrls: ['./table-detail.component.css']
})
export class TableDetailComponent implements OnInit {

  @Input() links: Link[];

  @Output() actionClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  clickRow(link: Link): void {
    this.actionClick.emit(link);
  }

}
