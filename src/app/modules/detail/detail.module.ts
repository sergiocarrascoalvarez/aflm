import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { DetailRoutingModule } from './detail-routing.module';

// Shared
import { SharedModule } from '../shared/shared.module';

// Components
import { MainDetailComponent } from './views/main-detail/main-detail.component';
import { TableDetailComponent } from './components/table-detail/table-detail.component';
import { InfoDetailComponent } from './components/info-detail/info-detail.component';

@NgModule({
  declarations: [MainDetailComponent, TableDetailComponent, InfoDetailComponent],
  imports: [
    CommonModule,
    DetailRoutingModule,
    SharedModule
  ]
})
export class DetailModule { }
