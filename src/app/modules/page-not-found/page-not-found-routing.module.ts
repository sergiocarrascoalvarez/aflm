import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Components
import { MainPageNotFoundComponent } from './views/main-page-not-found/main-page-not-found.component';

const routes: Routes = [
  {
    path: '',
    component: MainPageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PageNotFoundRoutingModule { }
