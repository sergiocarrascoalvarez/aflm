import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MainPageNotFoundComponent } from './main-page-not-found.component';
import { RouterTestingModule } from '@angular/router/testing';

describe('MainPageNotFoundComponent', () => {
  let component: MainPageNotFoundComponent;
  let fixture: ComponentFixture<MainPageNotFoundComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [ MainPageNotFoundComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MainPageNotFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
