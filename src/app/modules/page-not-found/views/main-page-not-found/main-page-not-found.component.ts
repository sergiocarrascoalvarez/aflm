import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-page-not-found',
  templateUrl: './main-page-not-found.component.html',
  styleUrls: ['./main-page-not-found.component.css']
})
export class MainPageNotFoundComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
