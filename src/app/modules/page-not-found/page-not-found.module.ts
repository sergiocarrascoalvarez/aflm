import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

// Routing
import { PageNotFoundRoutingModule } from './page-not-found-routing.module';

// Components
import { MainPageNotFoundComponent } from './views/main-page-not-found/main-page-not-found.component';

@NgModule({
  declarations: [MainPageNotFoundComponent],
  imports: [
    CommonModule,
    PageNotFoundRoutingModule
  ]
})
export class PageNotFoundModule { }
