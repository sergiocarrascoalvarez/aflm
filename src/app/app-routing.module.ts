import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: './modules/home/home.module#HomeModule',
    data: { animation: 'isLeft' }
  },
  {
    path: 'home',
    loadChildren: './modules/home/home.module#HomeModule',
    data: { animation: 'isLeft' }
  },
  {
    path: 'detail',
    loadChildren: './modules/detail/detail.module#DetailModule',
    data: { animation: 'isRight' }
  },
  {
    path: 'rules',
    loadChildren: './modules/page-rules/page-rules.module#PageRulesModule',
    data: { animation: 'isRight' }
  },
  {
    path: '**',
    loadChildren: './modules/page-not-found/page-not-found.module#PageNotFoundModule',
    data: { animation: 'isRight' }
   }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
