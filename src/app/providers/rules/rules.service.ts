import { Injectable } from '@angular/core';
import { Pages } from 'src/app/entities/pages';

@Injectable({
  providedIn: 'root'
})
export class RulesService {

  private visitedPages: Pages;

  constructor() {

    this.visitedPages = {
      animals: false,
      books: false,
      music: false,
      health: false
    };
  }

  getVisitedPages(): Pages {
    return this.visitedPages;
  }

  newVisitedPage(category: string): void {
    this.visitedPages[category] = true;
  }
}
