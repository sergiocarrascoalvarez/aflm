import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }

  getLinks(category: string): Observable<any> {
    return this.http.get(`https://api.publicapis.org/entries?category=${category}`);
  }

}
