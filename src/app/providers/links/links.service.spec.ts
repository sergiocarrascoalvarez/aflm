import { TestBed } from '@angular/core/testing';

import { LinksService } from './links.service';
import { ApiService } from '../api/api.service';
import { HttpClientModule } from '@angular/common/http';

describe('LinksService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientModule
    ],
    providers: [
      LinksService,
      ApiService
    ]
  }));

  it('should be created', () => {
    const service: LinksService = TestBed.get(LinksService);
    expect(service).toBeTruthy();
  });
});
