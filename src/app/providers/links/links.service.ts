import { Injectable } from '@angular/core';
import { ApiService } from '../api/api.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Link } from 'src/app/entities/link';

@Injectable({
  providedIn: 'root'
})
export class LinksService {

  constructor(private apiService: ApiService) { }

  getLinks(category: string): Observable<any> {
    return this.apiService.getLinks(category).pipe(
      map((response) => {
        const data = response.entries;
        const links: Link[] = [];

        if (data) {
          data.forEach(item => {
            const link: Link = {
              name: item.API,
              description: item.Description,
              url: item.Link,
              author: 'Desconocido'
            };
            links.push(link);
          });
        }

        return links;
      })
    );
  }

}
