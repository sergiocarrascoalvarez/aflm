import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RulesService } from '../providers/rules/rules.service';

@Injectable({
  providedIn: 'root'
})
export class RulesGuard implements CanActivate {

  constructor(
    private rulesService: RulesService,
    private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const visitedPages = this.rulesService.getVisitedPages();
    if (next.data.category === 'animals') {
      return true;
    } else if (next.data.category === 'books' && visitedPages.animals) {
      return true;
    } else if (next.data.category === 'music' && visitedPages.animals && visitedPages.books) {
      return true;
    } else if (next.data.category === 'healt' && visitedPages.animals && visitedPages.books && visitedPages.music) {
      return true;
    } else {
      this.router.navigate(['/rules']);
      return false;
    }

  }
}
