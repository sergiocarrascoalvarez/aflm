import { TestBed, async, inject } from '@angular/core/testing';

import { RulesGuard } from './rules.guard';
import { RouterTestingModule } from '@angular/router/testing';

describe('RulesGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      providers: [RulesGuard]
    });
  });

  it('should ...', inject([RulesGuard], (guard: RulesGuard) => {
    expect(guard).toBeTruthy();
  }));
});
